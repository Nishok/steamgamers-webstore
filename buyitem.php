<?php 
	include 'core/init.php';
	include 'includes/overall/header.php';
	protect_page();

	if(isset($_GET['item']) && !empty($_GET['item'])) {
		if(itemid_exists($_GET['item']) && $_GET['item'] != 1) {
			array_walk($item_data, 'array_sanitize');
			//Add item purchase to the logs
			//Add item to the user's profile
			if(steamid_active($user_data['steamid'])) {
				//Convert Item to Store ItemID
				//Add item purchase to the logs
				//Add item to the user's profile
				echo "Item <b>" . $item_data['itemname'] . "</b> bought! (still todo).";
			}
			else {
				echo "<i>You have not activated your SteamID yet. Item not bought.</i>";
			}
		}
		else {
			//echo "That item does not exist!";
			header('Location: index.php');
			exit();
		}
	}
	else {
		header('Location: index.php');
		exit();
	}

	include 'includes/overall/footer.php';
?>