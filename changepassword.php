<?php 
	include 'core/init.php';
	protect_page();
	
	if(empty($_POST) == false) {
		$required_fields = array('current_password', 'password', 'password2');
		foreach($_POST as $key=>$value) {
			if(empty($value) && in_array($key, $required_fields) == true) {
				$errors[] = 'Fields marked with an asterisk (*) are required!';
				break 1;
			}
		}
		if(MD5(MD5($_POST['current_password'].$user_data['salt'])) != $user_data['password']) {
			$errors[] = 'Your current password is incorrect!';
		}
		if($_POST['current_password'] == $_POST['password']) {
			$errors[] = 'Your new password can not be the same as your old password.';
		}
		if(strlen($_POST['password']) < 6) {
			$errors[] = 'Your new password must have a minimum length of 6 characters.';
		}
		if($_POST['password'] != $_POST['password2']) {
			$errors[] = 'Your new passwords do not match.';
		}
	}

	if(isset($_GET['success']) && empty($_GET['success'])) {
		include 'includes/overall/header.php'; 
		echo '<h1>Change password.</h1>
		Your password has been successfully changed.';
	}
	else {
		if(empty($_POST) == false && empty($errors) == true) {
			change_password($session_user_id, $_POST['password']);
			header('Location: changepassword.php?success');
			exit();
		}
			include 'includes/overall/header.php'; 
		if(empty($errors) == false) {
			echo '<h1>Change password.</h1>' . output_errors($errors);
		}
		//include 'includes/overall/header.php'; 
	// I moved the bracket '}' of the else statement to the footer, so the password form will be hidden when they submit with a success.
?>
		<h1>Change password.</h1>
		<form action="" method="post">
			<fieldset style="width:300px;">
				<legend>Change Password:</legend>
				<ul>
					<li>
						Current Password*:<br>
						<input type="password" name="current_password">
					</li>
					<li>
						New Password*:<br>
						<input type="password" name="password">
					</li>
					<li>
						Re-enter Password*:<br>
						<input type="password" name="password2">
					</li>
					<li>
						<input type="submit" value="Change Password">
					</li>
				</ul>
			</fieldset>
		</form>
			
<?php 
	} //This is the bracket from the else statement of the form.
include 'includes/overall/footer.php'; ?>