<?php 
	include 'core/init.php';
	protect_page();
	admin_page();
	
	if(empty($_POST) == false) {
		$required_fields = array('itemname', 'category', 'price', 'image', 'description', 'rank', 'store_id');
		foreach($_POST as $key=>$value) {
			if(empty($value) && in_array($key, $required_fields) == true) {
				$errors[] = 'Fields marked with an asterisk (*) are required!';
				break 1;
			}
		}
		if(empty($errors) == true) {
			if(strlen($_POST['itemname']) < 3 || strlen($_POST['itemname']) > 32) {
				$errors[] = 'The name of the item must be between 3 and 32 characters.';
			}
			if(item_exists($_POST['itemname']) == true) {
				$errors[] = 'The item ' . $_POST['itemname'] . ' already exists!';
			}
			$priceData = trim($_POST['price'], '0123456789');
			if(!empty($priceData)) {
				$errors[] = 'The price must be a number but can not be negative or contain decimals!';
			}
			if(strlen($_POST['description']) > 1024) {
				$errors[] = 'The description can only have a maximum of 1024 characters.';
			}
			/*if(checkRemoteFile($_POST['image']) == true) {
				list($width, $height) = getimagesize($_POST['image']); //Check for image size
				if($width != 200 && $height != 200) {
					$errors[] = 'The image size must be exact 200x200 pixels.';
				}
			}
			else {
				$errors[] = 'That is not a valid image!';
			}*/
			$allowedExts = array('.jpg', '.jpeg', '.png', '.gif');
			$maxFileSize = '2097152';
			$fileName = $_FILES['image']['name'];
			$ext = substr($fileName, strpos($fileName, '.'), strlen(fileName) - 1);
			if(filesize($_FILES['image']['tmp_name']) > $maxFileSize) {
				$errors[] = "The file size is too large, it must be less than 2MB.";
			}
			if(!in_array($ext, $allowedExts)) {
				$errors[] = "The file type you attempted to upload is not allowed.";
			}
			
			/*if(checkRemoteFile($_POST['image']) == false){
				$errors[] = 'That is not a valid image!';
			}*/
			$storeIDData = trim($_POST['store_id'], '0123456789');
			if(!empty($storeIDData)) {
				$errors[] = 'The Item ID must be a number but can not be negative or contain decimals!';
			}
			if(storeid_exists($_POST['store_id']) == true) {
				$errors[] = 'The Item ID ' . $_POST['store_id'] . ' already exists!';
			}
		}
	}
	
	if(isset($_GET['item_success']) && empty($_GET['item_success'])) {
		include 'includes/overall/header.php'; 
		echo "<h1>Admin</h1>
			Item successfully added!";
	}
	else {
		if(empty($_POST) == false && empty($errors) == true) {
			$item_data = array(
				'itemname'		 => $_POST['itemname'],
				'category'		 => $_POST['category'],
				'price'			 => $_POST['price'],
				'image'			 => "images/items/" . $_FILES['image']['name'],
				'description'	 => $_POST['description'],
				'rank'			 => $_POST['rank'],
				'store_id'		 => $_POST['store_id'],
				'uploader'		 => $user_data['username'],
				'ip'	 		 => getIP()
			);
			move_uploaded_file($_FILES['image']['tmp_name'], "./images/items/" . $_FILES['image']['name']);
			addItem($item_data);
			header('Location: admin.php?item_success');
			exit();
		}
		include 'includes/overall/header.php'; 
		if(empty($errors) == false) {
			echo "<h1>Admin</h1>" . output_errors($errors);
		}
?>
		<h1>Admin</h1>
		<form action="" method="post" enctype="multipart/form-data">
			<fieldset style="width:300px;">
				<legend>Adding Items</legend>
				<ul>
					<li>
						<b>Name*:</b><br>
						<input type="text" name="itemname" value="<?php if(isset($_POST['itemname'])) {echo sanitize($_POST['itemname']);} ?>">
					</li>
					<li>
						<b>Category*:</b><br>
						<select name="category">
							<option <?php if(isset($_POST['category']) && $_POST['category'] == 'Skins') {echo 'selected="selected"';} ?>>Skins</option>
							<option value="Skin_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Skin_Color') {echo 'selected="selected"';} ?>>Skin Color</option>
							<option <?php if(isset($_POST['category']) && $_POST['category'] == 'Hats') {echo 'selected="selected"';} ?>>Hats</option>
							<option <?php if(isset($_POST['category']) && $_POST['category'] == 'Trails') {echo 'selected="selected"';} ?>>Trails</option>
							<option value="Trail_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Trail_Color') {echo 'selected="selected"';} ?>>Trail Color</option>
							<option value="Weapon_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Weapon_Color') {echo 'selected="selected"';} ?>>Weapon Color</option>
							<option value="Spawn_Effect" <?php if(isset($_POST['category']) && $_POST['category'] == 'Spawn_Effect') {echo 'selected="selected"';} ?>>Spawn Effect</option>
							<option value="Nade_Trail" <?php if(isset($_POST['category']) && $_POST['category'] == 'Nade_Trail') {echo 'selected="selected"';} ?>>Nade Trail</option>
							<option value="Smoke_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Smoke_Color') {echo 'selected="selected"';} ?>>Smoke Color</option>
							<option value="Bullet_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Bullet_Color') {echo 'selected="selected"';} ?>>Bullet Color</option>
							<option value="Flashbang_Color" <?php if(isset($_POST['category']) && $_POST['category'] == 'Flashbang_Color') {echo 'selected="selected"';} ?>>Flashbang Color</option>
							<option value="Laser_Eyes" <?php if(isset($_POST['category']) && $_POST['category'] == 'Laser_Eyes') {echo 'selected="selected"';} ?>>Laser Eyes</option>
							<option value="Permanent_Score" <?php if(isset($_POST['category']) && $_POST['category'] == 'Permanent_Score') {echo 'selected="selected"';} ?>>Permanent Score #</option>
							<option value="Permanent_Death" <?php if(isset($_POST['category']) && $_POST['category'] == 'Permanent_Death') {echo 'selected="selected"';} ?>>Permanent Deaths #</option>
							<option value="Big_Head" <?php if(isset($_POST['category']) && $_POST['category'] == 'Big_Head') {echo 'selected="selected"';} ?>>Big Heads</option>
							<option <?php if(isset($_POST['category']) && $_POST['category'] == 'Pets') {echo 'selected="selected"';} ?>>Pets</option>
						</select>
					</li>
					<li>
						<b>Price (Credits)*:</b><br>
						<input type="text" name="price" value="<?php if(isset($_POST['price'])) {echo sanitize($_POST['price']);} ?>">
					</li>
					<li>
						<b>Image (2MB Max)*:</b><br>
						<!-- <input type="text" name="image" value="<?php //if(isset($_POST['image'])) {echo sanitize($_POST['image']);} ?>"> -->
						<input type="file" name="image" id="image">
					</li>
					<li>
						<b>Short Description*:</b><br>
						<textarea name="description" rows="5" cols="35"><?php if(isset($_POST['description'])) {echo sanitize($_POST['description']);} ?></textarea>
					</li>
					<li>
						<b>Rank-Only*:</b><br>
						<select name="rank">
							<option value="1" <?php if(isset($_POST['rank']) && $_POST['rank'] == 0) {echo 'selected="selected"';}?>>Everyone</option>
							<option value="2" <?php if(isset($_POST['rank']) && $_POST['rank'] == 1) {echo 'selected="selected"';}?>>Supporters</option>
							<option value="3" <?php if(isset($_POST['rank']) && $_POST['rank'] == 2) {echo 'selected="selected"';}?>>ServerAdmins</option>
						</select>
					</li>
					<li>
						<b>Item ID (Get this from the server's items list!)*:</b><br>
						<input type="text" name="store_id" value="<?php if(isset($_POST['store_id'])) {echo sanitize($_POST['store_id']);} ?>">
					</li>
					<li>
						<input type="submit" value="Add Item">
					</li>
				</ul>
			</fieldset>
		</form>
		
<?php 
	}
	include 'includes/overall/footer.php';
?>
<script type="text/javascript">
	function getFileSize(maxFileSize) {
		var input = document.getElementById("image");
		if(input.files && input.files.length == 1) {
			if(input.files[0].size > maxFileSize) {
				alert("The file size must be less than " + (maxFileSize/1024/1024) + "MB");
				return false;
			}
		}
		return true;
	}
</script>