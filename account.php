<?php 
	include 'core/init.php';
	protect_page();
	
	if(empty($_POST) == false) {
		$required_fields = array('email', 'steamid', 'steamid_code');
		foreach($_POST as $key=>$value) {
			if(empty($value) && in_array($key, $required_fields) == true) {
				$errors[] = 'Fields marked with an asterisk (*) are required!';
				break 1;
			}
		}
		if(empty($errors) == true) {
			if(!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false) {
				$errors[] = "A valid email is required.";
			}
			else if(!empty($_POST['email']) && email_exists($_POST['email']) == true && $user_data['email'] != $_POST['email']) {
				$errors[] = 'The email \'' . $_POST['email'] . '\' already exists.';
			}
			if(!empty($_POST['steamid']) && steamid_exists($_POST['steamid']) == true && $user_data['steamid'] != $_POST['steamid']) {
					$errors[] = 'The SteamID \'' . $_POST['steamid'] . '\' already exists!';
			}
			if(!empty($_POST['steamid_code']) && steamid_code_exists($_POST['steamid_code'], $user_data['steamid']) == false) {
					$errors[] = 'That code is incorrect!';
			}
		}
	}
 
	if(isset($_GET['success']) == true && empty($_GET['success']) == true) {
		include 'includes/overall/header.php'; 
		echo "<h1>Account</h1>
			Your details have been updated!";
	}
	else {
		if(empty($_POST['email']) == false && empty($_POST['steamid']) == false && empty($errors) == true) {
			if (showSteamID($user_data['steamid']) != $_POST['steamid']) {
				MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `steamid_active` = '0', `steamid_code` = '' WHERE `steamid` = '" . $user_data['steamid'] . "'");
			}
			$update_data = array(
				'email' 		=> $_POST['email'],
				'steamid' 		=> steamIDToNum($_POST['steamid']),
				'public'		=> $_POST['public']
			);
			update_user($update_data);
			header('Location: account.php?success');
			exit();
		}
		else if(empty($_POST['steamid_code']) == false && empty($errors) == true) {
			MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `steamid_active` = '1' WHERE `steamid` = '" . $user_data['steamid'] . "'");
			header('Location: account.php?success');
			exit();
		}
		include 'includes/overall/header.php'; 
		if(empty($errors) == false) {
			echo '<h1>Account</h1>' . output_errors($errors);
		}
	// I moved the bracket '}' of the else statement to the footer, so the account info will be hidden when they submit with a success.
		echo '<h1>Account</h1>';
		if($user_data['steamid_active'] != 1) {
			echo "
			<form action='' method='post'>
				<fieldset style='width:300px;'>
					<legend>SteamID Activation:</legend>
					<ul>
						<li>
							<b>Code* <a href='#' title='Go into the server, type !register and write the given code here.'>|?|</a>:</b><br>
							<input type='text' name='steamid_code'>
						</li>
						<li>
							<input type='submit' value='Activate'>
						</li>
					</ul>
				</fieldset>
			</form>";
		}
		?>
		<br>
		<fieldset style="width:300px;">
			<legend>Personal Information:</legend>
			<ul>
				<li>
					<b>Email:</b> <?php echo $user_data['email'];?>
				</li>
				<li>
					<b>SteamID:</b> <?php echo showSteamID($user_data['steamid']); if($user_data['steamid_active'] == 1) {echo ' | <i>Verified</i>';}?>
				</li>
				<li>
					<b>Public Profile <a href="#" title="Do you want your credits to be shown?">|?|</a>:</b> <?php if($user_data['public'] == 1) {echo 'Yes';} else {echo 'No';}?>
				</li>
				<li>
					<b>Credits:</b> <i>Still to add</i>
				</li>
			</ul>
		</fieldset>
		<br>
		<form action="" method="post">
			<fieldset style="width:300px;">
				<legend>Change Personal Information:</legend>
				<ul>
					<li>
						<b>Email*:</b><br>
						<input type="text" name="email" value="<?php echo $user_data['email'];?>">
					</li>
					<li>
						<b>SteamID*:</b> (Requires Re-Activation!)<br>
						<input type="text" name="steamid" value="<?php echo showSteamID($user_data['steamid']);?>">
					</li>
					<li>
						<b>Public Profile <a href="#" title="Do you want your credits to be shown?">|?|</a>:</b> <?php if($user_data['public'] == 1) {echo '<input type="checkbox" name="public" value="1" checked>';} else {echo '<input type="checkbox" name="public" value="1"';}?>
					</li>
					<li>
						<input type="submit" value="Save Changes">
					</li>
					<li>
						
					</li>
				</ul>
			</fieldset>
		</form>
		<br>
		<fieldset style="width:300px;">
			<legend>Password</legend>
			<ul>
				<li>
					<a href="changepassword.php">Change Password</a>
				</li>
			</ul>
		</fieldset>
	<?php 
	} //This is the bracket from the else statement of the form.
	include 'includes/overall/footer.php';
?>