<?php 
	include 'core/init.php';
	logged_in_redirect();

	if(isset($_GET['success']) == true && empty($_GET['success']) == true) {
		include 'includes/overall/header.php';
		echo "<h1>Activation Successful</h1>
			Your account has been successfully activated!";
	}
	else if(isset($_GET['email']) == true && $_GET['email_code'] == true) {
		$email = $_GET['email'];
		$code = $_GET['email_code'];
		
		if(email_exists($email) == false) {
			$errors[] = 'Email is invalid or does not exist!';
		}
		else if(activate($email, $code) == 2) {
			$errors[] = 'You have already activated your account!';
		}
		else if(activate($email, $code) == 3) {
			$errors[] = 'We were unable to activate your account.';
		}
		if(empty($errors) == false) {
			include 'includes/overall/header.php';
			echo '<h1>Activation Error</h1>' . output_errors($errors);
		}
		else {
			header('Location: activate.php?success');
			exit();
		}
	}
	else {
		header('Location: index.php');
	}
	
	include 'includes/overall/footer.php';
?>