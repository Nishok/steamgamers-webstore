<?php
	checkCredentialsCookie();
?>
<div class="widget">
	<h2><b>Hello, <?php echo $user_data['username'];?>!</b></h2>
	<div class="inner">
		<ul>
			<li>
				<a href="profile.php?username=<?php echo $user_data['username']; ?>"><img class='icon' src='./images/icons/vcard.png' alt=''/>Profile.</a> <?php if($user_data['public'] != 1) { echo '(Private)'; } ?>
			</li>
			<li>
				<b>Credits:</b> <?php if(steamid_active($user_data['steamid'])) {echo $user_store_data['credits'] . " <img class='icon' src='./images/icons/coins.png' alt=''/>";} else { echo "<a href='account.php'>Activate your SteamID!</a>";} ?>
			</li>
			<li>
				<b>SteamID:</b> <?php echo showSteamID($user_data['steamid']); ?>
			</li>
			
			<?php if(IsAdmin($session_user_id) == true) { echo "
			<li>
				<a href='admin.php'><img class='icon' src='./images/icons/lock.png' alt=''/>Admin Page</a>
			</li>
			"; } ?>
			<li>
				<a href="logout.php"><img class='icon' src='./images/icons/key_delete.png' alt=''/>Log Out.</a>
			</li>
		<ul>
	</div>
</div>