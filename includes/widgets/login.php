<div class="widget">
	<h2>Log in/Register</h2>
	<div class="inner">
		<?php 
			confirmIPAddress(getIP());
			if (confirmIPAddress(getIP())) {
				echo '<span style="color:red;font-weight:bold;">Too many login attempts! <br> 15 minutes cooldown!</span>';
			}
			else {
				echo "<form action='login.php' method='post'>
						<ul id='login'>
							<li>
								<b>Username:</b><br>
								<input type='text' name='username'>
							</li>
							<li>
								<b>Password:</b><br>
								<input type='password' name='password'>
							</li>
							<li>
								<input type='hidden' name='redirect' value='" . $_SERVER['REQUEST_URI'] . "'>
								<input type='submit' value='Log In'>
							</li>
							<li>
								<a href='register.php'>Register</a>
							</li>
							<li>
								Forgotten <a href='recover.php?mode=username'>username</a> or <a href='recover.php?mode=password'>password</a>?
							</li>
						</ul>
					</form>";
			}
		?>
	</div>
</div>