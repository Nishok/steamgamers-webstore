<?php 
	include 'core/init.php';
	include 'includes/overall/header.php'; 
	
	if(isset($_GET['username']) == true && empty($_GET['username']) == false) {
		$username = $_GET['username'];
		if(user_exists($username)) {
			$user_id = user_id_from_username($username);
			$profile_data = user_data($user_id, 'steamid', 'email', 'public');
			$store_data = user_store_data($profile_data['steamid'], 'credits', 'name');
			//if($profile_data['public'] == 1 || $session_user_id == $user_id) {
			if($profile_data['public'] == 1) {
				?>
				<h1><?php echo $username; ?>'s Profile</h1>
				<fieldset style='width:600px;'>
					<legend>Profile Information:</legend>
					<ul>
						<li>
							<b>SteamID:</b>  <?php echo showSteamID($profile_data['steamid']); ?>
						</li>
						<li>
							<b>Ingame Name:</b>  <?php if(steamid_active($profile_data['steamid'])) {echo $store_data['name'];} else { echo "<i>User has not activated their SteamID yet.</i>";} ?>
						</li>
						<li>
							<b>Credits:</b> <?php if(steamid_active($profile_data['steamid'])) {echo $store_data['credits'];} else { echo "<i>User has not activated their SteamID yet.</i>";} ?>
						</li>
						<li>
							<b>Page URL:</b> <input type="text" value="<?php echo curPageURL(); ?>">
						</li>
						<li>
							<b>Items:</b> <?php 
											if(steamid_active($profile_data['steamid'])) {
												//$stuff = array('18', '9', '17', '10', '0', '9', '18');
												$items = userStoreItemDetails(userStoreToMainItem($profile_data['steamid']));
												//$items = userStoreItemDetails($stuff);
												echo '<table class="items_table">';
												echo '<tr>';
												$index = 0;
												foreach($items as $key=>$value) {
													echo "<td>
													<div class='itemContainingBlock'>
														<div class='itemsName'>" . $value['itemname'] . "</div>
														<a href='items.php?item=" . $value['item_id'] . "'><div class='items_detail_button'>Details</div></a>
														<img src='" . $value['image'] . "'>
													</div>
													</td>";
													$index += 1;
													if ($index % 3 == 0) {
														echo '</tr>';
														echo '<tr>';
													}
													//echo "<fieldset style='width:200px;'>" . $value['itemname'] . " | " . $value['category'] . "</fieldset><br>";
												}
												echo '</tr>';
												echo '</table>';
											}
											else {
												echo "<i>User has not activated their SteamID yet.</i>";
											}
										?>
						</li>
					</ul>
				</fieldset>
				<?php
			}
			else {
				echo '<h1>' . $username . '\'s Profile</h1>';
				$errors[] = "Sorry, this profile is private.";
			}
		}
		else {
			$errors[] = "The user '" . $username . "' does not exist!";
		}
		if(empty($errors) == false) {
			echo "<h1>" . $username . "'s Profile</h1>" . output_errors($errors);
		}
	}
	else {
		header('Location: index.php');
		exit();
	}
	
	include 'includes/overall/footer.php';
?>