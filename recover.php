<?php 
	include 'core/init.php';
	logged_in_redirect();
	
	if(isset($_GET['success']) && empty($_GET['success'])) {
		include 'includes/overall/header.php'; 
		echo "<h1>Recover</h1>
		We have sent you an email!";		
	}
	else if(isset($_GET['pass_success']) && empty($_GET['pass_success'])) {
		include 'includes/overall/header.php'; 
		echo "<h1>Recover</h1>
		Your password has been successfully changed. We have sent you an email with your new password!";
	}
	else {
		$mode_allowed= array('username', 'password');
		if(isset($_GET['mode']) && in_array($_GET['mode'], $mode_allowed)) {
			if(isset($_POST['email']) && !empty($_POST['email'])) {
				if(email_exists($_POST['email'])) {
					recover($_GET['mode'], $_POST['email']);
					header('Location: recover.php?success');
					exit();
				}
				else {
					$errors[] = 'The email \'' . $_POST['email'] . '\' does not exist!';
				}
			}
			include 'includes/overall/header.php';
			if(empty($errors) == false) {
				echo '<h1>Recover Error</h1>';
				echo output_errors($errors);
			}
			else {
				echo '<h1>Recover</h1>';	
			}
			?>
				<form action="" method="post">
					<fieldset style="width:300px;">
						<legend>Account Recovery:</legend>
						<ul>
							<li>
								<b>Please enter your email address:</b><br>
								<input type="text" name="email" value="<?php if(isset($_POST['email'])) {echo $_POST['email'];} ?>">
							</li>
							<li>
								<input type="submit" value="Recover">
							</li>
						</ul>
					</fieldset>
				</form>
			<?php
		}
		else if(isset($_GET['email']) && $_GET['pass_code']) {
			$email = $_GET['email'];
			$pass_code = $_GET['pass_code'];
			
			if(email_exists($email) == false) {
				$errors[] = 'Email is invalid or does not exist!';
			}
			if(pass_code_exists($pass_code) == false) {
				$errors[] = 'This link is invalid!';
			}
			if(empty($errors) == false) {
				include 'includes/overall/header.php';
				echo '<h1>Recover Error</h1>';
				echo output_errors($errors);
			}
			else if(empty($errors) == true) {
				$user_data = user_data(user_id_from_email($email), 'username', 'salt');
				$length = 8;
				$generated_password = base64_encode(mcrypt_create_iv(ceil(0.75*$length), MCRYPT_DEV_URANDOM));
				$password = MD5(MD5($generated_password.$user_data['salt']));
				MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `password` = '$password' WHERE `email` = '$email'");
				MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `pass_code` = '' WHERE `email` = '$email'");
				email($email, 'Password Recovery', "
					Hello " . $user_data['username'] . ",
					
					You have successfully changed your password.
					
					Here is your new password: " . $generated_password . "
					
					Please do not forget to change your password!
					
					-----
					Sincerely,
					
					The Steam Gamers Staff.
					http://www.steamgamers.com			
				");
				header('Location: recover.php?pass_success');
				exit();
			}
		}
		else {
			header('Location: index.php');
			exit();
		}
	}
	
	include 'includes/overall/footer.php';
?>