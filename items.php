<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<?php
	include 'core/init.php';
	include 'includes/overall/header.php'; 
	
	if(!isset($_GET['page']) || $_GET['page'] < 1 || !is_numeric($_GET['page'])) {
		$page = 1;
	}
	else {
		$page = (int)$_GET['page'];
	}
	
	if(isset($_GET['category']) && !empty($_GET['category'])) {
		echo "<h1><a href='". $_SERVER['PHP_SELF'] . "'>Categories</a> &raquo; <a href='". $_SERVER['PHP_SELF'] . "?category=" . $_GET['category'] . "'>" . $_GET['category'] . "</a> &raquo; Page " . $page . "</h1>";
		if($_GET['category'] == 'All Items') {
			$items = fetchAllItemsPage($page);
			echo '<table class="items_table">';
			echo '<tr>';
			$index = 0;
			while($row = mysql_fetch_assoc($items)) {
					if(!file_exists($row['image'])) { $row['image'] = 'images/items/no-image.jpg'; }
					list($width, $height) = getimagesize($row['image']);
					$imageSize = resizeImage(200, 200, $width, $height);
					echo "<td>
					<div class='itemContainingBlock'>
						<div class='itemsName'>" . $row['itemname'] . "</div>
						<a href='". $_SERVER['PHP_SELF'] . "?item=" . $row['item_id'] . "'><div class='items_detail_button'>Details</div></a>
						<div class='itemImageCenter'><img src='" . $row['image'] . "' width='" . $imageSize['width'] . "' height='" . $imageSize['height'] . "'></div>
					</div>
					</td>";
					$index += 1;
					if ($index % 3 == 0 && $index != 6) {
						echo '</tr>';
						echo '<tr>';
					}
			}
			echo '</tr>';
			echo '</table>';
			
			echo '<div>';
				if($page > 1) {
					echo '<a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . ($page - 1) . '">Prev</a> | ';
				}
				else {
					echo 'Prev | ';
				}
				for ($i=1; $i<=totalPages(); $i++) { 
					echo '<a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . $i . '">' . $i . '</a> '; 
				}
				if(totalPages($_GET['category']) > $page) { 
					echo '| <a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . ($page + 1) . '">Next</a>';   
				}
				else {
					echo '| Next';
				}
			echo '</div>';
		}
		else {
			if(category_exists($_GET['category']) == true) {
				$items = fetchItemsPage($page, $_GET['category']);
				echo '<table class="items_table">';
				echo '<tr>';
				$index = 0;
				while($row = mysql_fetch_assoc($items)) {
						if(!file_exists($row['image'])) { $row['image'] = 'images/items/no-image.jpg'; }
						list($width, $height) = getimagesize($row['image']);
						$imageSize = resizeImage(200, 200, $width, $height);
						if($row['category'] == $_GET['category']) {
							echo "<td>
							<div class='itemContainingBlock'>
								<div class='itemsName'>" . $row['itemname'] . "</div>
								<a href='". $_SERVER['PHP_SELF'] . "?item=" . $row['item_id'] . "'><div class='items_detail_button'>Details</div></a>
								<div class='itemImageCenter'><img src='" . $row['image'] . "' width='" . $imageSize['width'] . "' height='" . $imageSize['height'] . "'></div>
							</div>
							</td>";
							$index += 1;
							if ($index % 3 == 0 && $index != 6) {
								echo '</tr>';
								echo '<tr>';
							}
						}
				}
				echo '</tr>';
				echo '</table>';
				
				echo '<div>';
					if($page > 1) {
						echo '<a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . ($page - 1) . '">Prev</a> | ';
					}
					else {
						echo 'Prev | ';
					}
					for ($i=1; $i<=totalCategoryPages($_GET['category']); $i++) { 
						echo '<a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . $i . '">' . $i . '</a> '; 
					}
					if(totalCategoryPages($_GET['category']) > $page) { 
						echo '| <a href="' . $_SERVER['PHP_SELF'] . '?category=' . $_GET['category'] . '&page=' . ($page + 1) . '">Next</a>';  
					}
					else {
						echo '| Next';
					}
				echo '</div>';
			}
			else {
				echo 'Sorry, there are no items in that category!';
			}
		}
	}
	else if(isset($_GET['item']) && !empty($_GET['item'])) {
		if(itemid_exists($_GET['item'])) {
			?>
			<div>
				<form id="buyForm" action="buyitem.php">
					<input type="hidden" name="item" value="<?php echo $_GET['item']; ?>"/>
				</form>
			</div>
			<div>
				<form id="deleteForm" onsubmit='return confirm("Are you sure you want to delete this item?")' action="deleteitem.php">
					<input type="hidden" name="item" value="<?php echo $_GET['item']; ?>"/>
				</form>
			</div>
			<div class="itemDisplay">
				<?php
				if(!file_exists($item_data['image'])) { $item_data['image'] = 'images/items/no-image.jpg'; }
				list($width, $height) = getimagesize($item_data['image']);
				$imageSize = resizeImage(200, 200, $width, $height);
				if(logged_in() == true) { if(steamid_active($user_data['steamid']) == false) { echo "<div class='itemDisplayData itemDisplayInactiveSteamID itemDisplayWarning'>You have not yet activated your SteamID!<br>Click here to activate it so you can buy items!</div>"; } } else { echo "<div class='itemDisplayData itemDisplayWarning'>You are not logged in!<br>Please <a href='register.php'>register</a> or <a href='index.php'>login</a> to buy items.</div>"; }
				echo "
				<div class='itemDisplayData itemDisplayName'>" . $item_data['itemname'] . "</div>
				<div class='itemDisplayData'><img src='" . $item_data['image'] .  "' width='" . $imageSize['width'] . "' height='" . $imageSize['height'] . "'></div>
				<div class='itemDisplayData'>" . $item_data['description'] . "</div>";
				if(logged_in() == true) { 
					if(steamid_active($user_data['steamid']) /*&& !userHasItem($_GET['item']) */) { 
						echo "<div class='itemDisplayData itemDisplayPrice buy'>Buy: " . $item_data['price'] . " <img class='icon' src='./images/icons/coins.png' alt=''/></div>"; 
					} 
					else { 
						echo "<div class='itemDisplayData itemDisplayPrice itemDisplayPriceInactive'>Buy: " . $item_data['price'] . " <img class='icon' src='./images/icons/coins.png' alt=''/></div>";
					} 
				}
				else { 
					echo "<div class='itemDisplayData itemDisplayPrice itemDisplayPriceInactive'>Buy: " . $item_data['price'] . " <img class='icon' src='./images/icons/coins.png' alt=''/></div>";
				}?>
				<?php if(logged_in() == true) {if(IsAdmin($user_data['user_id']) == true) { echo "
				<div class='itemDisplayData'>" . $item_data['uploader'] . "</div>
				<div class='itemDisplayData itemDisplayAdmin'>Delete Item</div>"; } } ?>
			</div>
			<?php
		}
		else {
			echo 'This item does not exist!';
		}
	}
	else {
		echo '<h1>Select Category</h1>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=All Items">All Categories</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Skins">Skins</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Skin_Color">Skin Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Hats">Hats</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Trails">Trails</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Trail_Color">Trail Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Weapon_Color">Weapon Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Spawn_Effect">Spawn Effect</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Nade_Trail">Nade Trail</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Smoke_Color">Smoke Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Bullet_Color">Bullet Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Flashbang_Color">Flashbang Color</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Laser_eyes">Laser Eyes</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Permanent_Score">Permanent Score #</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Permanent_Death">Permanent Deaths #</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Big_Head">Big Heads</a><br>';
		echo '<a href="'. $_SERVER['PHP_SELF'] . '?category=Pets">Pets</a>';
	}
	
	include 'includes/overall/footer.php';
?>
<script>
	$(".buy").click(function() {
		$("#buyForm").submit();
	});
	$(".itemDisplayAdmin").click(function() {
		$("#deleteForm").submit();
	});
	$(".itemDisplayInactiveSteamID").click(function() {
		window.location = 'account.php';
	});
</script>