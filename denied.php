<?php 
	include 'core/init.php';

	if(logged_in() == false) {
		include 'includes/overall/header.php'; 
		echo "
			<h1>You need to be logged in to see this page.</h1>
			<p>Please <a href='register.php'>register</a> or <a href='index.php'>login</a>.</p>";
	}
	else {
		header('Location: index.php');
		exit();
	}

	include 'includes/overall/footer.php';
?>