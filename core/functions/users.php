<?php
	function user_data($user_id) {
		$data = array();
		$user_id = (int)$user_id;
		$func_num_args = func_num_args();
		$func_get_args = func_get_args();
		
		if($func_num_args > 1) {
			unset($func_get_args[0]);
			
			$fields = '`' . implode('`, `', $func_get_args) . '`';
			$data = mysql_fetch_assoc(MySqlDatabase::getInstance()->queryMain("SELECT $fields FROM `users` WHERE `user_id` = $user_id"));
			
			return $data;
		}
	}
	
	function user_store_data($steamID) {
		$data = array();
		$steamID = sanitize($steamID);
		$func_num_args = func_num_args();
		$func_get_args = func_get_args();
		
		if($func_num_args > 1) {
			unset($func_get_args[0]);
			
			$fields = '`' . implode('`, `', $func_get_args) . '`';
			$data = mysql_fetch_assoc(MySqlDatabase::getInstance()->queryStore("SELECT $fields FROM `combinedstore` WHERE `id` = $steamID"));
			
			return $data;
		}
	}
	
	function user_store_items($steamID) {
		$steamID = sanitize($steamID);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT `item` FROM `itemlist` WHERE `id` = '$steamID'");
		$itemsID = array();
		while($row = mysql_fetch_assoc($query)) {
			$itemsID[] = $row['item'];
		}
		return $itemsID;
	}
	
	function userStoreToMainItem($steamID) {
		$steamID = sanitize($steamID);
		$query1 = MySqlDatabase::getInstance()->queryStore("SELECT `item` FROM `itemlist` WHERE `id` = '$steamID'");
		$itemsID = array();
		$id = 0;
		while($row = mysql_fetch_assoc($query1)) {
			$query2[] = MySqlDatabase::getInstance()->queryMain("SELECT `item_id` FROM `items` WHERE `store_id` = '" . $row['item'] . "'");
			if(storeid_exists($row['item'])) {
				$itemsID[] = mysql_result($query2[$id], 0);
			}
			else {
				$itemsID[] = '1';
			}
			$id++;
		}
		return $itemsID;
	}
	
	function userStoreItemDetails($itemID) {
		array_walk($itemID, 'array_sanitize');
		foreach($itemID as $value) {
			$item_details[] = item_data($value, 'item_id', 'itemname', 'category', 'price', 'image', 'description', 'uploader', 'ip');
		}
		return $item_details;
	}
	
	function user_count() {
		return mysql_result(MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `active` = 1"), 0);
	}
	
	function register_user($register_data) {
		array_walk($register_data, 'array_sanitize');
		$register_data['password'] = MD5(MD5($register_data['password'].$register_data['salt']));
		
		$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
		$data = '\'' . implode('\', \'', $register_data) . '\'';
		
		MySqlDatabase::getInstance()->queryMain("INSERT INTO `users`($fields) VALUES ($data)");
		email($register_data['email'], 'Account Activation', "
			Hello " . $register_data['username'] . ",
			
			Thanks for registering. Please activate your account by click the link below:
			
			http://therealnishok.com/webshop/activate.php?email=" . $register_data['email'] . "&email_code=" . $register_data['email_code'] . "
			
			-----
			Sincerely,
			
			The Steam Gamers Staff.
			http://www.steamgamers.com			
		");
	}
	
	function activate($email, $email_code) {
		$email = mysql_real_escape_string($email);
		$code = mysql_real_escape_string($email_code);
		
		if(mysql_result(MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `email` = '$email' AND `email_code` = '$email_code' AND `active` = 0"), 0) == 1) {
			MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `active` = 1 WHERE `email` = '$email'");
			return 1;
		}
		else if(mysql_result(MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `email` = '$email' AND `email_code` = '$email_code' AND `active` = 1"), 0) == 1) {
			return 2;
		}
		else {
			return 3;
		}
	}
	
	function change_password($user_id, $password) {
		$user_id = (int)$user_id;
		$salt = create_salt();
		$password = MD5(MD5($password.$salt));
		
		MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `salt` = '$salt' WHERE `user_id` = $user_id");
		MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `password` = '$password' WHERE `user_id` = $user_id");
	}
	
	function recover($mode, $email) {
		$mode = sanitize($mode);
		$email = sanitize($email);
		
		$user_data = user_data(user_id_from_email($email), 'username', 'pass_code', 'email');
		
		if($mode == 'username') {
			email($email, 'Username Recovery', "
			Hello " . $user_data['username'] . ",
			
			It seems you have forgotten your username, so here it is: " . $user_data['username'] . ".
			
			-----
			Sincerely,
			
			The Steam Gamers Staff.
			http://www.steamgamers.com			
		");
		}
		else if($mode == 'password') {
			//Send them a link to generate a random password.
			$pass_code = MD5($user_data['username'] + microtime());
			MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `pass_code` = '$pass_code' WHERE `email` = '$email'");
			email($email, 'Password Recovery', "
				Hello " . $user_data['username'] . ",
				
				You have requested to reset your password because you have forgotten your password. If you did not request this, please ignore it.
				
				To reset your password, please visit the following page:
				http://therealnishok.com/webshop/recover.php?email=" . $user_data['email'] . "&pass_code=" . $pass_code . "
				
				When you visit that page, your password will be reset, and the new password will be emailed to you.
				
				-----
				Sincerely,
				
				The Steam Gamers Staff.
				http://www.steamgamers.com			
			");
		}
	}
	
	function update_user($update_data) {
		$update = array();
		array_walk($update_data, 'array_sanitize');
		foreach($update_data as $field=>$data) {
			$update[] = '`' . $field . '` = \'' . $data . '\'';
		}
		
		MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET " . implode(', ', $update) . " WHERE `user_id` = " . $_SESSION['user_id']);
	}
	
	function logged_in() {
		return (isset($_SESSION['user_id'])) ? true : false;
	}
	
	function IsAdmin($user_id) {
		$user_id = (int)$user_id;
		return (mysql_result(MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `user_id` = $user_id AND `rank` = 1"), 0) == 1) ? true : false;
	}

	function user_exists($username) {
		$username = sanitize($username);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}

	function email_exists($email) {
		$email = sanitize($email);
		//$db = MySqlDatabase::getInstance();
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `email` = '$email'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function pass_code_exists($pass_code) {
		$pass_code = sanitize($pass_code);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `pass_code` = '$pass_code'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function steamid_exists($steamid) {
		$steamid = sanitize($steamid);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `steamid` = '$steamid'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function steamid_code_exists($steamid_code, $steamid) {
		$steamid_code = sanitize($steamid_code);
		$steamid = sanitize($steamid);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `steamid_code` = '$steamid_code' AND `steamid` = '$steamid'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function steamid_active($steamid) {
		$steamid = sanitize($steamid);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `steamid` = '$steamid' AND `steamid_active` = 1");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function user_active($username) {
		$username = sanitize($username);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username' AND `active` = 1");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function user_id_from_username($username) {
		$username = sanitize($username);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT `user_id` FROM `users` WHERE `username` = '$username'");
		return mysql_result($query, 0, 'user_id');
	}
	
	function user_id_from_email($email) {
		$email = sanitize($email);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT `user_id` FROM `users` WHERE `email` = '$email'");
		return mysql_result($query, 0, 'user_id');
	}
	
	function steamIDToNum($steamID) {
		$steamID = sanitize($steamID);
		$steamIDPieces = explode(":", $steamID);
		return ($steamIDPieces[2] << 1) | $steamIDPieces[1];
	}
	
	function showSteamID($steamID) {
		$steamID = sanitize($steamID);
		$steamIDNum = $steamID / 2;
		$steamIDAuthNum = $steamID & 1;
		return "STEAM_0:" . $steamIDAuthNum . ":" . $steamIDNum;
	}
	
	function login($username, $password) {
		$user_id = user_id_from_username($username);
		$username = sanitize($username);
		$salt = mysql_result(MySqlDatabase::getInstance()->queryMain("SELECT salt FROM `users` WHERE `username` = '$username'"), 0);
		$password = MD5(MD5($password.$salt));
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username' AND `password` = '$password'");
		return (mysql_result($query, 0) == 1) ? $user_id : false;
	}
	
	function logout() {
		session_start();
		if(isset($_COOKIE['credentials'])) {
			setcookie('credentials', '', time()-7000000);
		}
		session_destroy();
		header('Location: index.php');
		exit;
	}
	
	function isValid($str) {
		return !preg_match('/[^A-Za-z0-9.#\\-$]/', $str);
	}
	
	function getIP() {
		$ip = $_SERVER['REMOTE_ADDR'];
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ip = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		}
		return $ip;
	}
	
	function setCookieSalt($user_id, $salt) {
		$user_id = (int)$user_id;
		$salt = sanitize($salt);
		MySqlDatabase::getInstance()->queryMain("UPDATE `users` SET `cookie_salt` = '$salt' WHERE `user_id` = $user_id");		
	}
	
	function getCookieSalt($user_id) {
		$user_id = (int)$user_id;
		$query = MySqlDatabase::getInstance()->queryMain("SELECT `cookie_salt` FROM `users` WHERE `user_id` = '$user_id'");
		return mysql_result($query, 0, 'cookie_salt');	
	}
	
	function confirmIPAddress($value) {
		$locktime = 15;
		$attempts = 3;
		$query = "SELECT attempts, (CASE when lastlogin is not NULL and DATE_ADD(lastlogin, INTERVAL " . $locktime . " MINUTE)>NOW() then 1 else 0 end) as Denied FROM `loginattempts` WHERE ip = '$value'";
		$result = MySqlDatabase::getInstance()->queryMain($query);
		$data = mysql_fetch_array($result);
		if (!$data) {
			return 0;
		}
		if ($data["attempts"] >= $attempts) {
			if($data["Denied"] == 1) {
				return 1;
			}
			else {
				clearLoginAttempts($value);
				return 0;
			}
		}
		return 0;
	}
	
	function addLoginAttempt($value) {
		$query = "SELECT * FROM loginattempts WHERE ip = '$value'";
		$result = MySqlDatabase::getInstance()->queryMain($query);
		$data = mysql_fetch_array($result);
		if($data) {
			$attempts = $data["attempts"]+1;
			if($attempts==3) {
				$query = "UPDATE loginattempts SET attempts=" . $attempts . ", lastlogin=NOW() WHERE ip = '$value'";
				$result = MySqlDatabase::getInstance()->queryMain($query);
			}
			else {
				$query = "UPDATE loginattempts SET attempts=" . $attempts . " WHERE ip = '$value'";
				$result = MySqlDatabase::getInstance()->queryMain($query);
			}
		}
		else {
			$query = "INSERT INTO loginattempts (attempts,IP,lastlogin) values (1, '$value', NOW())";
			$result = MySqlDatabase::getInstance()->queryMain($query);
		}
	}
	
	function clearLoginAttempts($value) {
		$query = "UPDATE loginattempts SET attempts = 0 WHERE ip = '$value'";
		return MySqlDatabase::getInstance()->queryMain($query);
	}
?>