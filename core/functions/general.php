<?php 
	function sanitize($data) {
		return htmlentities(strip_tags(mysql_real_escape_string($data)));
	}
	
	function array_sanitize(&$item) {
		$item = htmlentities(strip_tags(mysql_real_escape_string($item)));
	}
	
	function create_salt() {
		$length = 16;
		$iv = base64_encode(mcrypt_create_iv(ceil(0.75*$length), MCRYPT_DEV_URANDOM));
		return $iv;
	}
	
	function generateCredentialsCookie($user_id, $username) {
		$secretkey = 'g&%#Fedog*v$Up7kFw&@';
		$encrypted = encrypt($user_id.':'.$username, $secretkey);
		$cookie_salt = create_salt();
		$encrypted .= ':' . hash_hmac('sha256', $encrypted, $cookie_salt); //Encryption(user_id:username):hashed_encryption
		setCookieSalt($user_id, $cookie_salt);
		setcookie('credentials', $encrypted, time()+21600); //6H
	}
	
	function readCredentialsCookie() {
		if(isset($_COOKIE['credentials'])) {
			$secretkey = 'g&%#Fedog*v$Up7kFw&@';
			$parts = explode(':', $_COOKIE['credentials']); //Array ( [0] => encrypt(user_id:username) [1] => hash_hmac($encrypted) )
			$hashed_encrypt = array_pop($parts); //the hashed encryption
			$encrypted = implode(':', $parts); //needed incase mcrypt added `:` //Encryption(user_id:username):hash_hmac($encrypted)
			$raw = decrypt($encrypted, $secretkey); //Decrypt it
			list ($user_id, $username) = explode(':', $raw, 2);
			if ($hashed_encrypt == hash_hmac('sha256', $parts[0], getCookieSalt($user_id))) {
				//return array($user_id, $username); //Only in case if I need the username, for this system I don't need it
				return $user_id;
			}
		}
	}
	
	function checkCredentialsCookie() {
		if(isset($_COOKIE['credentials'])) {
			$secretkey = 'g&%#Fedog*v$Up7kFw&@';
			$parts = explode(':', $_COOKIE['credentials']);
			$hashed_encrypt = array_pop($parts);
			$encrypted = implode(':', $parts);
			$raw = decrypt($encrypted, $secretkey);
			list ($user_id, $username) = explode(':', $raw, 2);
			if ($hashed_encrypt != hash_hmac('sha256', $parts[0], getCookieSalt($user_id))) { //User modified the cookie
				logout();
			}
		}
		else { //Cookie expired or doesn't exist
			logout();
		}
	}
	
	function cookieToSession() {
		if(isset($_COOKIE['credentials'])) {
			$_SESSION['user_id'] = readCredentialsCookie();
		}
	}
	
	function encrypt($decrypted, $password, $salt='18*ikf5Q8wp8Lx!Em8g#') { //Encrypt + Decrypt credits go to http://www.php.net/manual/en/book.mcrypt.php#107483
		// Build a 256-bit $key which is a SHA256 hash of $salt and $password.
		$key = hash('SHA256', $salt . $password, true);
		// Build $iv and $iv_base64.  We use a block size of 128 bits (AES compliant) and CBC mode.  (Note: ECB mode is inadequate as IV is not used.)
		srand(); $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
		if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
		// Encrypt $decrypted and an MD5 of $decrypted using $key.  MD5 is fine to use here because it's just to verify successful decryption.
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));
		// We're done!
		return $iv_base64 . $encrypted;
	}

	function decrypt($encrypted, $password, $salt='18*ikf5Q8wp8Lx!Em8g#') {
		// Build a 256-bit $key which is a SHA256 hash of $salt and $password.
		$key = hash('SHA256', $salt . $password, true);
		// Retrieve $iv which is the first 22 characters plus ==, base64_decoded.
		$iv = base64_decode(substr($encrypted, 0, 22) . '==');
		// Remove $iv from $encrypted.
		$encrypted = substr($encrypted, 22);
		// Decrypt the data.  rtrim won't corrupt the data because the last 32 characters are the md5 hash; thus any \0 character has to be padding.
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
		// Retrieve $hash which is the last 32 characters of $decrypted.
		$hash = substr($decrypted, -32);
		// Remove the last 32 characters from $decrypted.
		$decrypted = substr($decrypted, 0, -32);
		// Integrity check.  If this fails, either the data is corrupted, or the password/salt was incorrect.
		if (md5($decrypted) != $hash) return false;
		// Yay!
		return $decrypted;
	}
	
	function output_errors($errors) {
		return '<span style="color:red;font-weight:bold;"><ul><li>' . implode('</li><li>', $errors) . '</li></ul></span>';
	}
	
	function protect_page() {
		if(logged_in() == false) {
			header('Location: denied.php');
			exit();
		}
	}
	
	function admin_page() {
		global $user_data;
		if(IsAdmin($user_data['user_id']) == false) {
			header('Location: index.php');
			exit();
		}
	}
	
	function logged_in_redirect() {
		If(logged_in() == true) {
			header('Location: index.php');
			exit();
		}
	}
	
	function email($to, $subject, $body) {
		mail($to, $subject, $body, 'From: noreply@therealnishok.com');
	}
	
	function curPageURL() {
		$pageURL = 'http';
		if ($_SERVER['SERVER_PORT'] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}
		else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	function checkRemoteFile($url) { // Checks if remote file exists.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		// don't download content
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(curl_exec($ch) !== false) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function addItem($item_data) {
		array_walk($item_data, 'array_sanitize');
		
		$fields = '`' . implode('`, `', array_keys($item_data)) . '`';
		$data = '\'' . implode('\', \'', $item_data) . '\'';
		
		MySqlDatabase::getInstance()->queryMain("INSERT INTO `items`($fields) VALUES ($data)");
	}
	
	function item_exists($itemname) {
		$itemname = sanitize($itemname);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`item_id`) FROM `items` WHERE `itemname` = '$itemname'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function itemid_exists($item_id) {
		$item_id = sanitize($item_id);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`item_id`) FROM `items` WHERE `item_id` = '$item_id'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function storeid_exists($store_id) {
		$store_id = sanitize($store_id);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`store_id`) FROM `items` WHERE `store_id` = '$store_id'");
		return(mysql_result($query, 0) == 1) ? true : false;
	}
	
	function category_exists($category) {
		$category = sanitize($category);
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT(`category`) FROM `items` WHERE `category` = '$category'");
		return(mysql_result($query, 0) >= 1) ? true : false;
	}
	
	function fetchAllItemsPage($page) {
		$page = ($page - 1) * 6;
		$query = MySqlDatabase::getInstance()->queryMain("SELECT * FROM items WHERE `category` != 'hidden' LIMIT $page, 6");
		return $query;
	}
	
	function fetchItemsPage($page, $category) {
		$page = ($page - 1) * 6;
		$query = MySqlDatabase::getInstance()->queryMain("SELECT * FROM `items` WHERE `category` = '$category' LIMIT $page, 6");
		return $query;
	}
	
	function totalPages() {
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT('item_id') FROM `items`");
		$row = mysql_fetch_row($query);
		$row[0] -= 1; //Because we have 1 invisible item "Unknown Item"
		return ceil($row[0] / 6);
	}
	
	function totalCategoryPages($category) {
		$query = MySqlDatabase::getInstance()->queryMain("SELECT COUNT('item_id') FROM `items` WHERE `category` = '$category'");
		$row = mysql_fetch_row($query);
		return ceil($row[0] / 6);
	}
	
	function item_data($item_id) {
		$data = array();
		$item_id = (int)$item_id;
		$func_num_args = func_num_args();
		$func_get_args = func_get_args();
		
		if($func_num_args > 1) {
			unset($func_get_args[0]);
			
			$fields = '`' . implode('`, `', $func_get_args) . '`';
			$data = mysql_fetch_assoc(MySqlDatabase::getInstance()->queryMain("SELECT $fields FROM `items` WHERE `item_id` = $item_id"));
			
			return $data;
		}
	}
	
	function resizeImage($goalWidth, $goalHeight, $width, $height) {
		$return = array('width' => $width, 'height' => $height);
		
		if($width / $height > $goalWidth / $goalHeight && $width > $goalWidth) {
			$return['width'] = $goalWidth;
			$return['height'] = $goalWidth / $width * $height;
		}
		else if($height > $goalHeight) {
			$return['width'] = $goalHeight / $height * $width;
			$return['height'] = $goalHeight;
		}
		
		//$return = array('width' => 200, 'height' => 200);
		return $return;
	}
?>