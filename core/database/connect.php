<?php 
	//$connect_error = 'Sorry, we are having connection issues with the database. Please contact an administrator.';
	//$mainDB = mysql_connect('localhost', 'root', 'usbw') or die($connect_error);
	//$storeDB = mysql_connect('66.150.121.74', 'root', '0Ha1th3r3') or die($connect_error);
	
	//mysql_select_db('webshop', $mainDB) or die($connect_error);
	//mysql_select_db('webshop', $storeDB) or die($connect_error);	
	
	class MySqlDatabase {
		public $linkMain;
		public $linkStore;
		
		private $conn_str;
		private static $instance;
		private function __construct() {}

		public function connectMain($host, $user, $password, $database=false, $persistant=false) {
			if ($persistant) {
				$this->linkMain = @mysql_pconnect($host, $user, $password, true);
			}
			else {
				$this->linkMain = @mysql_connect($host, $user, $password, true);
			}
			
			if (!$this->linkMain) {
				throw new Exception('Unable to establish database connection: ' . mysql_error());
			}

			if ($database) $this->useDatabaseMain($database);
			
			$version = mysql_get_server_info();
			$this->conn_str = "'$database' on '$user@$host' (MySQL $version)";
			
			return $this->linkMain;
		}
		
		public function connectStore($host, $user, $password, $database=false, $persistant=false) {
			if ($persistant) {
				$this->linkStore = @mysql_pconnect($host, $user, $password, true);
			}
			else {
				$this->linkStore = @mysql_connect($host, $user, $password, true);
			}
			
			if (!$this->linkStore) {
				throw new Exception('Unable to establish database connection: ' . mysql_error());
			}

			if ($database) $this->useDatabaseStore($database);
			
			$version = mysql_get_server_info();
			$this->conn_str = "'$database' on '$user@$host' (MySQL $version)";
			
			return $this->linkStore;
		}
		
		public static function getInstance() {
			if (!isset(self::$instance)) {
				self::$instance = new MySqlDatabase();
			}
			
			return self::$instance;
		}
		
		public function queryMain($query) {
			$r = @mysql_query($query, $this->linkMain);

			if (!$r) {
				throw new Exception("Query Error: " . mysql_error());
			}
			
			return $r;
		}
		
		public function queryStore($query) {
			$r = @mysql_query($query, $this->linkStore);

			if (!$r) {
				throw new Exception("Query Error: " . mysql_error());
			}
			
			return $r;
		}
		
		public function useDatabaseMain($database) {
			if (!@mysql_select_db($database, $this->linkMain)) {
				throw new Exception('Unable to select database: ' . mysql_error($this->linkMain));
			}
		}
		
		public function useDatabaseStore($database) {
			if (!@mysql_select_db($database, $this->linkStore)) {
				throw new Exception('Unable to select database: ' . mysql_error($this->linkStore));
			}
		}
	}
?>