<?php 
	session_start();
	//error_reporting(0); //EDIT THIS AND MAKE IT UNCOMMENTED.
	
	include_once 'database/connect.php';
	include_once 'functions/users.php';
	include_once 'functions/general.php';
	
	// get an instance of the Database singleton
    $dbInstance = MySqlDatabase::getInstance();
	$db = null;
	$db2 = null;
    //$db2 = MySqlDatabase::getInstance();
    
    // connect to a MySQL database (use your own login information)
    try {
		$db = $dbInstance->connectMain('localhost', 'root', 'usbw', 'webshop');
    } 
    catch (Exception $e) {
		die($e->getMessage());
    }
	
    try {
		//$db2 = $dbInstance->connectStore('66.150.121.74', 'root', '0Ha1th3r3', 'combinedstore');
		$db = $dbInstance->connectStore('localhost', 'root', 'usbw', 'combinedstore');
    } 
    catch (Exception $e) {
		die($e->getMessage());
    }
	
	cookieToSession();
	if(logged_in() == true) {
		$session_user_id = $_SESSION['user_id'];
		$user_data = user_data($session_user_id, 'user_id', 'username', 'password', 'pass_code', 'steamid', 'steamid_active', 'steamid_code', 'email', 'public', 'rank', 'salt');
		if(user_active($user_data['username']) == false) {
			logout();
		}
		if(steamid_active($user_data['steamid'])) {
			$user_store_data = user_store_data($user_data['steamid'], 'credits', 'skincolor', 'spawneffect', 'nadetrail', 'smokecolor', 'paintball', 'tracer', 'pets', 'flashbang', 'score', 'death', 'head', 'trail', 'trailcolor', 'weaponcolor', 'skin', 'hat', 'name');
		}
	}
	if(isset($_GET['item']) && !empty($_GET['item'])) {
		$item_id = $_GET['item'];
		$item_data = item_data($item_id, 'itemname', 'category', 'price', 'image', 'description', 'uploader', 'ip');
	}
	
	$errors = array();
?>