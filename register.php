<?php 
	include 'core/init.php';
	logged_in_redirect(); 
	
	if(empty($_POST) == false) {
		$required_fields = array('username', 'password', 'password2', 'steamid', 'email');
		foreach($_POST as $key=>$value) {
			if(empty($value) && in_array($key, $required_fields) == true) {
				$errors[] = 'Fields marked with an asterisk (*) are required!';
				break 1;
			}
		}
		if(empty($errors) == true) {
			if(user_exists($_POST['username']) == true) {
				$errors[] = 'The username \'' . $_POST['username'] . '\' already exists.';
			}
			if(isValid($_POST['username']) == false) {
				$errors[] = 'Your username contains invalid characters.';
			}
			if(strlen($_POST['password']) < 6) {
				$errors[] = 'Your password must have a minimum length of 6 characters.';
			}
			if($_POST['password'] != $_POST['password2']) {
				$errors[] = 'Your passwords do not match.';
			}
			if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false) {
				$errors[] = "A valid email is required.";
			}
			if(email_exists($_POST['email']) == true) {
				$errors[] = 'The email \'' . $_POST['email'] . '\' already exists.';
			}
		}
	}
	
	if(isset($_GET['register_success']) && empty($_GET['register_success'])) {
		include 'includes/overall/header.php';
		echo '<h1>Register</h1>
		You have been successfully registered! Please check your email to activate your account.';
	}
	else {	
		if(empty($_POST) == false && empty($errors) == true) {
			if($_POST['public'] == 1) {
				$public = 1;
			}
			else {
				$public = 0;
			}
			
			$register_data = array(
				'username' 		=> $_POST['username'],
				'password' 		=> $_POST['password'],
				'register_ip' 	=> getIP(),
				'steamid' 		=> steamIDToNum($_POST['steamid']),
				'email' 		=> $_POST['email'],
				'email_code'	=> MD5($_POST['username'] + microtime()),
				'public'		=> $public,
				'salt' 			=> create_salt()
			);
			register_user($register_data);
			header('Location: register.php?register_success');
			exit();
		}
		include 'includes/overall/header.php';
		if(empty($errors) == false) {
			echo "<h1>Register</h1>" . output_errors($errors);
		}
	// I moved the bracket '}' of the else statement to the footer, so the register form will be hidden when they submit with a success.
?>
		<h1>Register</h1>
		<form action="" method="post">
			<ul>
				<li>
					<b>Username*:</b><br>
					<input type="text" name="username" value="<?php if(isset($_POST['username'])) {echo sanitize($_POST['username']);} ?>">
				</li>
				<li>
					<b>Password*:</b> (Minimum 6 characters)<br>
					<input type="password" name="password">
				</li>
				<li>
					<b>Re-enter Password*:</b><br>
					<input type="password" name="password2">
				</li>
				<li>
					<b>SteamID*:</b> (STEAM_0:1:23456789)<br>
					<input type="text" name="steamid" value="<?php if(isset($_POST['steamid'])) {echo sanitize($_POST['steamid']);} ?>">
				</li>
				<li>
					<b>Email*:</b><br>
					<input type="text" name="email" value="<?php if(isset($_POST['email'])) {echo sanitize($_POST['email']);} ?>">
				</li>
				<li>
				<li>
					<b>Public Profile <a href="#" title="Do you want your credits to be shown?">|?|</a>:</b> <input type="checkbox" name="public" <?php if(isset($_POST['public'])) {if($_POST['public'] == '0') {echo "value='0'";} else { echo "value='1' checked"; }} else { echo "value='1' checked"; } ?>>
				</li>
					<input type="submit" value="Register">
				</li>
			</ul>
		</form>
			
<?php 
	} //This is the bracket from the else statement of the form.
	include 'includes/overall/footer.php';
?>