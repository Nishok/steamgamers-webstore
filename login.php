<?php
	include 'core/init.php';
	logged_in_redirect();
	
	if(empty($_POST) == false) {
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		if(empty($username) == true || empty($password) == true) {
			$errors[] = 'You need to enter a username and password.';
		}
		else if(user_exists($username) == false) {
			$errors[] = 'User does not exist!';
		}
		else if(user_active($username) == false) {
			$errors[] = 'You have not activated your account yet.';
		}		
	}
	
	if(empty($_POST) == false && empty($errors) == true) {
		$login = login($username, $password);
		if($login == false) {
			$errors[] = 'That username/password combination is incorrect.';
			addLoginAttempt(getIP());
		}
		else {
			clearLoginAttempts(getIP());
			generateCredentialsCookie($login, $username);
			$_SESSION['user_id'] = $login;
			//header('Location: index.php');
			if(isset($_POST['redirect'])) {
				//header("Location: " . $_POST['redirect']);
				?><script type="text/javascript">window.location = "<?php echo $_POST['redirect']; ?>"</script><?php
			}
			exit();
		}
	}
	if(empty($errors) == false) {
		include 'includes/overall/header.php';
		echo "<h1>Login</h1>" . output_errors($errors);
		include 'includes/overall/footer.php';
	}
?>